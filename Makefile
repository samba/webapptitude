# LDFLAGS := -L/usr/local/opt/openssl/lib
CFLAGS := -I/usr/local/opt/openssl/include
LDFLAGS=$(shell pkg-config openssl --cflags --libs)
SRCDIR=webapptitude

GIT_COMMIT = $(shell git log -1 "--pretty=format:%H")
GIT_BRANCH = $(shell git describe --contains --all HEAD)
GIT_STATUS = $(shell git status -sb --untracked=no | wc -l | awk '{ if($$1 == 1){ print "clean" } else { print "pending" } }')
PYTHON_VERSION := "v$(shell python setup.py --version)"
VERSION_PATTERN = "^v[0-9]+\.[0-9]+\.[0-9]+([abc][0-9]*)?$$"
TEST_MATCH=

# For consistency with GitLab's CI exposing the secrets...
GOOGLE_API_SECRETS := $(shell base64 -i test/creds/webapptitude-5e6e5d16392a.json)
export GOOGLE_API_SECRETS TEST_MATCH

system_dependencies:
ifeq ($(shell uname),Darwin)
	which brew && brew install openssl libjpeg
else
	which apt-get && apt-get install libjpeg-dev
endif

.virtualenv: requirements.txt requirements_test.txt | system_dependencies
	virtualenv $@ && \
		. ./.virtualenv/bin/activate && \
		pip install --upgrade pip && \
		LDFLAGS="$(LDFLAGS)" CFLAGS="$(CFLAGS)" pip install --upgrade -r requirements.txt && \
		pip install --upgrade -r requirements_test.txt
	touch -r $< $@


.coverage test:  .virtualenv
	. ./.virtualenv/bin/activate && \
		SKIP_ASYNC=$${SKIP_ASYNC:-1} python -m webapptitude.test  -f --coverage  -vvvv -t ./ test/


test_bigquery:  .virtualenv
	. ./.virtualenv/bin/activate && \
		SKIP_ASYNC=$${SKIP_ASYNC:-1} python -m webapptitude.test  -f \
			--pattern="test_bigquery*.py" \
			--coverage  -vvvv -t ./ test/

test_analytics:  .virtualenv
	. ./.virtualenv/bin/activate && \
		SKIP_ASYNC=$${SKIP_ASYNC:-1} python -m webapptitude.test  -f \
			--pattern="test_google_analytics*.py" \
			--coverage  -vvvv -t ./ test/


test_queue:  .virtualenv
	. ./.virtualenv/bin/activate && \
		SKIP_ASYNC=$${SKIP_ASYNC:-1} python -m webapptitude.test  -f \
			--pattern="test_queue*.py" \
			--coverage  -vvvv -t ./ test/


test_app:
	# . ./.virtualenv/bin/activate &&
	pip -q install --upgrade -t testapp/lib -r requirements.txt
	python `which dev_appserver.py` ./app.yaml

coverage: .coverage
	coverage report --include='webapptitude/*'

coverage_detail: .coverage
	coverage report -m \
		'--omit=/Library/*,/Applications/*,lib/*,bin/*,test/*,*google-cloud-sdk*,*/lib/python*' \
		--fail-under=70

package: .coverage
	echo ${PYTHON_VERSION} | egrep ${VERSION_PATTERN} || exit 1
	python setup.py check build sdist bdist


package-upload: update_version
	echo "DEPRECATED: use GitLab CI for test/build/deploy automation"
	test -f manual_package_upload && python setup.py check build sdist bdist upload


update_version:  # Apply tags based on version embedded in package.
	test ${GIT_STATUS} = "clean" || exit 1
	echo ${PYTHON_VERSION} | egrep ${VERSION_PATTERN} || exit 1
	python setup.py check sdist bdist
	git tag -d ${PYTHON_VERSION} || echo "(this version is new)"
	git tag ${PYTHON_VERSION}


clean:
	python setup.py clean
	find ./${SRCDIR} -name '*.pyc' -print -delete
	rm -rvf *.egg-info build/ dist/

clean_virtenv:
	virtualenv --clear .virtualenv
	sleep 1 && touch requirements.txt # So the virtenv will reinstall next time.

.PHONY: test clean coverage coverage_detail package
