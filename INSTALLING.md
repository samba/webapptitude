# Installation tips

## From PyPI packages

Hopefully this is the simplest approach.

```shell
pip install webapptitude
```

## Installing from source

### System Dependencies

I'd recommend installing these first, as some Python libraries require them during
build/installation.

#### On a Mac

```shell
which brew && brew install openssl libjpeg
```


#### On Linux (Debian)
```shell
which apt-get && apt-get install libjpeg62-turbo-dev libffi-dev libssl-dev
```

### Python installation

Some systems strugle with platform-build dependencies mixed with others. Occasionally
installing them in a specific order simplifies it. Recommendation below.

```shell
sudo pip install pillow
sudo python setup.py install
```

