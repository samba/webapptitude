"""
This configuration should be used ONLY in test environments.
It relies on the existing of a virtual environment (virtualenv) as provided by
this project's Makefile.

`appengine_config.py` is automatically loaded when Google App Engine
starts a new instance of your application. This runs before any
WSGI applications specified in app.yaml are loaded.
"""

import sys
from google.appengine.ext import vendor


def version_deprecated(module_name):
    """Move Google's path to the last, for a given module."""
    sort_sys_path(lambda x: not ((module_name in x) and ('appengine' in x)))


def sort_sys_path(evaluator):
    sys.path = sorted(sys.path,
                      key=evaluator,
                      reverse=True)


def unload_module(module_name):
    target_modules = [m for m in sys.modules if m.startswith(module_name)]
    for m in target_modules:
        if m in sys.modules:
            del sys.modules[m]


print >>sys.stderr, ("webapptitude test appengine_config.py loaded.")

# Preserve the priority of virtual environment
sort_sys_path(lambda x: ('.virtualenv' in x))

version_deprecated('oauth2client')
version_deprecated('pyasn1_modules')

# AppEngine ~1.9.38 bundles oauth2client==1.4.12, which lacks some important
# recent improvements, especially the ServiceAccountCredentials.
unload_module('oauth2client')
unload_module('pyasn1_modules')
