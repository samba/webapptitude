
# Basic Usage

Many of the features of this application are built around extending the core
`WSGIApplication` and `RequestHandler` constructs. These improvements extend
many existing features of `webapp2` to, in effect, enable them with sensible
defaults. 

The following example highlights several of the webapptitude's improvements.

```python

from webapptitude import WSGIApplication, RequestHandler
from webapptitude.util import 
from webapptitude.decorator import require, is_secure

app = WSGIApplication(debug=True)

@app.route('/')  # The application root
class MyRequestHandler(RequestHandler):

    def get(self):
        # Session support is built into all request handlers.
        # This writes a (string) ISO-8601 timestamp to the session.
        self.session['start'] = str(util.datetime8601())
        # Strings returned will be regarded as response body.
        return 'Welcome to my app!'


@app.route('/template/<keyname>')
class TemplateRequestHandler(RequestHandler):
    
    @require(is_secure)  # Enforce that the request must be over HTTPS.
    def post(self, keyname=None):
        # RequestHandler provides a shortcut to accessing request parameters, 
        # either form-encoded, url-encoded on the URI, or passed as JSON body.
        title = self.input.get('title', 'NO_TITLE')
        # The RequestHandler provides several template handling shortcuts.
        # This is the most obviously useful.
        return self.respond_with_template("templates/example.html", 
                                          keyname=keyname, title=title)

```

## URI Routing

Webapptitude provides an easy method on the `WSGIApplication` to register new
routes, in a declarative style with the `RequestHandler` definition.

There are two modes of declaring routes, assuming `app` is a `WSGIApplication` instance:

- `@app.route({string template})` as a decorator on a class definition; this is most useful when the application is defined wholely in a single module, and permits a very declarative style.
- `app.route({string template}, {class derives_RequestHandler})` attach an existing class. This can be useful when attaching handlers loaded from other modules.

Both forms accept named arguments:

- `name="{string route_name}"`  a name to use for this route when building URIs later.
- `schemes=["http", "https"]`  or a subset thereof, constrains route matching
- `methods=["get", "put", "post"...]`  match only these methods

These arguments are passed directly to [`webapp2.Route`][3].

Named routes can be accessed within templates as well: the Jinja2 environment has
access to `url_for` as a function (aliased `uri_for`) to generate paths within
a template. Example:

```html
    <a href="{{ uri_for("dashboard") }}">Go to Dashboard.</a>
    <a href="{{ uri_for("project_home", project_id=123) }}">Project 123></a>
```

These would assume a routing configuration like:

```python
from webapptitude import WSGIApplication
from .project import HomeRequestHandler
from .dashboard import DashboardRequestHandler

app = WSGIApplication()

app.route('/project/<project_id>', HomeRequestHandler, name='project_home')
app.route('/dashboard', DashboardRequestHandler, name='dashboard')

```

[3]: http://webapp2.readthedocs.io/en/latest/api/webapp2.html#webapp2.Route


## Sessions

Webapp2 provides some great utilities for handling sessions; webapptitude simply 
enables them by default and provides a (very standard) shortcut.

Within a `webapptitude.RequestHandler` instance 
(derived from `webapptitude.handlers.SessionRequestHandler`),
the `self.session` property represents a dictionary-like object which is loaded
from the datastore on request, and saved back to the datastore upon completion.

Example usage:

```python
class RequestHandler(webapptitude.RequestHandler):

    def get(self):
        self.session['started'] = True

```
 
## Users

Google AppEngine provides its own `users` module, and webapptitude leverages it. Note that this relies on the Google Accounts infrastructure, which supports Google Apps domains.

Within a `webapptitude.RequestHandler`, the `self.user` property retrieves the
current user of any request. It will return `None` if the user is not logged in.

Several related methods are also exposed to the Jinja2 template logic as globals:

- `user` is an accessor to the logged in user's properties, e.g. `user.email()`
- `logout_url` can be used to generate links for the user to deauthorize from your application.
- `login_url` can be used to generate links for the user to authorize into your application.


Example:

```html
    <a href="{{ login_url('/profile') }}">Log in to Profile</a>
    <a href="{{ logout_url('/logged-out') }}">Log Out</a>
```
