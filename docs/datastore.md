
# Datastore Extensions

The module `webapptitude.models` provides several interesting models.

## Models with UUID keys

`UUIDKeyModel` (extends `ndb.Model`) constructs keys that are based on UUIDs. This can be useful when key identifiers must be distributed in URLs, or in other forms over the web, instead of the native `urlsafe` mechanism of NDB's key architecture.

Accessing the UUID of the key: `uuidmodel_instance.key.id()`


## Reliable User references in Google accounts

`UserReference` model provides several useful functions for storing user identifiers to Google accounts. In particular, because Google discourages reference by email address, as email addresses can theoretically change for a Google account, this model abstracts reference by the account ID, which is assured to be consistent.

- `UserReference.current()` fetches the UserReference instance for the _current_ user of the session.
- `UserReference.by_email({string email address})` attempts to resolve the user by email address using Google account, in the standard `users` module.
- `UserReference.for_user({users.User})` fetches the reference instance for a specific user, through the same `users` module standard.
- From a UserReference instance, `userref_instance.user_instance()` produces the `users.User` instance associated by email.

## User Properties

Often an application may need to store a user reference; this can be treated as a model property, using the `UserProperty` class, which proxies a for `UserReference` model.

Usage:

```python
from webapptitude import models

class Profile(models.Model):
    owner = models.UserProperty(auto_current_user_add=True)

```

## Models with Change-state tracking

Simple, but effective: automatic handling of create and modify timestamps.

```python
from webapptitude import models
from datetime import datetime

class Profile(models.TimestampModel):
    pass

now = datetime.now()

profile = Profile()
profile.put()

assert (profile.created_at == now)
assert (profile.updated_at == now)
```

This model also provides a property `created_by`, a `UserProperty` noted above, which
retains the user who created the entity.

## User-owned Profiles

Many applications will retain a group of entities that are "owned" by a specific user. This often includes user preferences. webapptitude provides a `UserProfile` model for this purpose. This model can be used without a derivative class, if needed.

```python

from webapptitude import models
from google.appengine.api import users

profile = models.UserProfile.current()

profile.preferences['color'] = 'orange'
profile.put()

```
