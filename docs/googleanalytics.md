# Google Analytics integration

This library provides an abstraction over the Google Analytics API v4. 
Its primary function is to provide an architecture of reusable components 
(e.g. segments) to populate automated reports, while performing some lightweight
validation as the report is built, to simplify resolution of common errors.

This document targets software developers, who may have limited understanding of
Google Analytics, but are working with stakeholders and analysts who are already
familiar with the lingo of Google Analytics. 
Eventually I'll provide an even higher level of abstraction over what's built
here, probably in the way of configuring report spec documents.

For now, the important parts:

* [Authorization](#authorization)
* [Discovering Data Sources](#discovering-data-sources)  (accounts, properties, views)
* [Developing Reports](#developing-reports)
* [Specifying the Date Range](#specifying-the-date-range)  (potentially multiple)
* [Configure Sampling](#configure-sampling)
* [Requesting Multiple Reports](#requesting-multiple-reports)
* [Reporting with Cohorts](#reporting-with-cohorts)
* [Reporting with Segments](#reporting-with-segments)
* [Sorting and Ordering a Report](#sorting-and-ordering-a-report)
* [Specifying Dimensions and Metrics](#specifying-dimensions-and-metrics)
* [Filtering a Report](#filtering-a-report)
* [Pivoting in a Report](#pivoting-in-a-report)
* [Fetching Results](#fetching-results)
* [Technical References](#references)

## Authorization

Accessing Analytics requires authorization as either the user, or as a service account.
Each requires private key data, identifying the application. These are regarded as "secret keys" in much of this documentation and codebase, and can be accessed through the Google Cloud Console.

Further:

- In Google AppEngine and Google Compute Engine, each machine can have access to the project's service account(s) by default.
- Web applications and Installed applications can rely on per-user authorization, wherein a user
approves the application to have access to the user's account, and act on behalf of the user. This requires secret keys, but initiates a "flow" that allows the user to delegate access.
- Other applications can access the service accounts with the appropriate private key data, in which the service account must be authorized (by email address) to a specific analytics data source. 

The `oauthkit` module provides several "factories" for simplifying access grants through these models. 

For this Analytics documentation, we'll rely on the `oauthkit.web` factory, which is appropriate for AppEngine installations.

The Google Analytics reporting service can be accessed like so:

```python
from webapptitude import googleanalytics
from webapptitude import oauthkit

auth = oauthkit.web()  # OAuth credentials factory.
ga = googleanalytics.GoogleAnalyticsReportingService(auth)  # the Google Analytics proxy.

```

## Discovering Data Sources

This library provides a few means to discover available accounts, properties, and views.

```python
# as above
ga = googleanalytics.GoogleAnalyticsReportingService(auth) 

# List available accounts
for id_, name, acct in ga.accounts:
    account_id = id_  # for reference later, the last account.
    print "Account found: %r = %s" % (id_, name)

# List properties available under a specific account
for id_, id_internal, name, prop in ga.list_properties(account_id):
    # Here the id_ is the full web property ID, e.g. "UA-XXXXX-Y"
    # The internal ID is required in most API query interfaces
    prop_id = id_internal
    print "Property found: %r = %s" % (id_, name)

# List views/profiles under a specific account+property
for id_, name, profile in ga.list_profiles(account_id, prop_id):
    # This id_ (the view ID) is required for building reports (later).
    print "Profile(view) found: %r = %s" % (id_, name)

# Or if you just want to see everything...
for profile in ga.profiles:  # a generator
    # Some useful properties are extracted below.
    # The source objects are also available as:
    # - profile.account (a dict)
    # - profile.webproperty (a dict)
    # - profile.profile (a dict)
    print "Found profile (account=%r, property=%r, view=%r)" % profile.ident
    print "Account %r = %s" % (profile.ident[0], profile.account_name)
    print "Property %r = %s" % (profile.ident[1], profile.webproperty_name)
    print "View %r = %s" % (profile.ident[2], property.profile_name)


```


## Developing Reports

Google Analytics API v4 operates in *batches*. Up to 5 reports are permitted in a single batch.

The `ReportBatch` class coordinates batch-level operations, including configuration of segments and cohorts. The `ReportRequestModel` class handles report-level operations, including pivots and filters. While perhaps not intuitive to seasoned Google Analytics experts, this breakdown (table below) enforces consistency in the report modeling per Google's API functionality. Several features must be consistent across all reports in a batch, and therefore are handled at the batch-level.

| Scope  | Function   |
|--------|------------|
| Batch  | Segments   |
| Batch  | Date range |
| Batch  | Sampling   |
| Batch  | Cohorts    |
| Report | Filters    |
| Report | Pivots     |
| Report | Sort/Order |

Most of these functions are best illustrated through pure code examples.


### Specifying the Date Range

Reports typically require specifying 1 or (as most) 2 date ranges; this allows comparison reports, e.g. month-over-month reporting. Note that cohort batches cannot have date-ranges applied.

```python
from webapptitude import googleanalytics
batch = googleanalytics.ReportBatch(view_id)
batch.add_date_range('2016-01-01', '2016-01-31')
```

### Configure Sampling

Sampling has one of three values:

- `DEFAULT`
- `SMALL`
- `LARGE`

These can be configured as:

```python
from webapptitude import googleanalytics
batch = googleanalytics.ReportBatch(view_id)
batch.set_sampling_level("LARGE")
```

### Reporting with Cohorts

Note that when reporting cohorts, a distinct group of dimensions and metrics is available;  standard dimensions and metrics are incompatible. Further, some cohort dimensions require specific date-range scales; for example, the `ga:cohortNthDay` dimension requires each cohort date-range to be 1-day in duration. 

```python
batch = googleanalytics.ReportBatch(view_id)
batch.add_cohort(
    # arguments:       {cohort name} {start date}  {end date}
    batch.build_cohort('2016-March', '2016-03-01', '2016-03-31'),
    batch.build_cohort('2016-April', '2016-04-01', '2016-04-30')
)
```

### Reporting with Segments

Segments allow reports to evaluate a group of dimensions and metrics against
various classes of users and their respective visits.

A segment represents a collection of visits, however it may select those based
on the history of the users who produce those visits. Each segment may define
multiple segment components in each class (user and session). 

When multiple segment components are provided, they're treated as "AND" qualifiers.
Sessions will be included by meeting *all* of the associated components; it's 
therefore necessary to avoid conflict, e.g. do not provide top-level segments
where, for example, `ga:medium` is "organic" AND `ga:medium` is "cpc".

Segments can also be composed of "simple" constraints, which evaluate several
properties of users' sessions in a single pass (i.e. no sequential constraints). 

Sequential segments are composed of steps, which reflect the process of a users'
experience on the site, either in terms of sessions and attribution, or in terms
of their viewing of particular content (i.e. hit-level).

Segments can further be composed of constraints either on metrics or dimensions.
When constraints are expressed against metrics, their scope must also be declared.

Metric scope can be one of:

- `USER`
- `SESSION`
- `HIT`
- `PRODUCT`

Metric operators can be one of:

- `EQUAL`
- `LESS_THAN`
- `GREATER_THAN`
- `BETWEEN`
- `IS_MISSING`

When using the `BETWEEN` operator, the compared value must be a list of two parameters.

Dimension operators can be one of:

- `EXACT`
- `REGEXP`
- `BEGINS_WITH`
- `ENDS_WITH`
- `PARTIAL`
- `NUMERIC_EQUAL`
- `NUMERIC_GREATER_THAN`
- `NUMERIC_LESS_THAN`
- `IN_LIST`

When using the `IN_LIST` operator, the compared value must be a list of one or more strings.

Sequence steps require a `match type` consisting of either `PRECEDES` or `IMMEDIATELY_PRECEDES`; 
these define the relationship to _the following_ step in the series. Further, the sequence
segment itself can accept a named argument `match_first`, which (when `True`) requires the 
first step to match the first hit/session of each user.


```python
batch = googleanalytics.ReportBatch(view_id)

# function aliases (shorter usage below)
dimseg = batch.build_segment_filter_dimension_clause  # shortcut
metseg = batch.build_segment_filter_metric_clause

paid = dimesg("ga:medium", 'EXACT', 'cpc')
organic = dimseg("ga:medium", 'EXACT', 'organic')
direct = dimseg("ga:medium", 'EXACT', '(direct)')
social_sources = ['facebook.com', 'twitter.com']
social = dimseg("ga:source", 'IN_LIST', social_sources)
nonbounce = metseg("ga:pageviews", "SESSION", "GREATER_THAN", 1)

batch.add_segment(
    batch.build_segment(
        "goodmarketing",

        # Attend only to the social non-bouncing visits
        sessions=[batch.build_simple_segment([social], [nonbounce])],

        # From users who have multiple marketing touch points...
        users=[batch.build_sequence_segment(

            # First a paid source
            batch.build_segment_sequence_step('PRECEDES', [paid]),

            # Then (eventually) a social source
            batch.build_segment_sequence_step('IMMEDIATELY_PRECEDES', [social]),

            # Then (immediately) an direct source
            batch.build_segment_sequence_step('PRECEDES', [direct]),

            match_first=True
        )]
    ),
    batch.build_segment(  # another segment, all sessions
        "all",
        sessions=[batch.build_simple_segment([
            dimseg(cls.LANDING_PAGE, 'REGEXP', '^/')
        ])]
    )
)
```

### Requesting Multiple Reports

Each batch permits up to 5 reports. The `ReportBatch` interface provides a simple
way to incorporate new reports. Typically you'll only need to use the `next_report()`
method to add another report to the batch, and access it directly.

This example demonstrates using three reports in a batch.

```python
from webapptitude import googleanalytics
batch = googleanalytics.ReportBatch(view_id)
first_report = batch.cursor  # this one's already populated
second_report = batch.next_report()  # create a new report request
third_report = batch.next_report() # ... and so on.

# the cursor always points at the last one.
assert (third_report is batch.cursor)  
assert isinstance(first_report, googleanalytics.ReportRequestModel)

```

The limit on total reports is enforced at `fetch` time; an `AssertionError` will
be thrown for most violations of these limits, as defined by Google's API. This
library simply preempts those violations with clearer messaging.

### Specifying Dimensions and Metrics

Each report will often require a distinct group of dimensions and metrics.

```python
batch = googleanalytics.ReportBatch(view_id)
report = batch.cursor  # access the "current" report

# Add multiple dimensions by name
report.add_dimension("ga:source", "ga:medium")

# Add a (numeric) dimension with a histogram bucket effect.
report.add_dimension("ga:sessionCount", histogram=["<5", "5-10", "10+"])

report.add_metric("ga:pageviews")

```


### Sorting and Ordering a Report

Note that when histograms are in use, the associated dimension is used as an
ordering field, and its ordering type is `HISTOGRAM_BUCKET`. 

Order type can be one of:

- `VALUE` (default)
- `DELTA`
- `SMART`
- `DIMENSION_INTEGER`
- `HISTOGRAM_BUCKET`

Sort order can be one of:

- `ASCENDING` (default)
- `DESCENDING`

```python
batch = googleanalytics.ReportBatch(view_id)
report = batch.cursor  # access the "current" report

report.apply_order("ga:source", order_type='VALUE', sort_order='ASCENDING')
```

### Filtering a Report

Metric filters can have operators:

- `EQUAL` (default)
- `LESS_THAN` 
- `GREATER THAN`
- `IS_MISSING`

Dimension filters can have operators:

- `REGEXP` (default)
- `EXACT`
- `IN_LIST` (allows a series of values)
- `BEGINS_WITH` 
- `ENDS_WITH`
- `PARTIAL`
- `NUMERIC_EQUAL`
- `NUMERIC_GREATER_THAN`
- `NUMERIC_LESS_THAN`

```python
batch = googleanalytics.ReportBatch(view_id)
report = batch.cursor  # access the "current" report

report.add_metric_filters([  # within this list, they'll be 
    report.build_metric_filter("ga:pageviews", "1", operator='GREATER_THAN')
])

report.add_dimension_filters(
    # Each clause listed here will be "AND"-ed together.
    # sessions coming from Google...
    report.build_dimension_filter_clause(
        # Each filter listed here is treated as a component of the same filter.
        report.build_dimension_filter("ga:source", "google", operator='REGEXP'),
        report.build_dimension_filter(...)
        operator='OR' # default, or "AND"
    ),
    report.build_dimension_filter_clause(...)
)

```

### Pivoting in a Report

Pivoting a report, in the API, selects one or more dimensions as _columns_ in the report, 
where their _values_ represent unique (pairs, groups) of column headers, and the selected
metrics populate a column related to each combination.

In other words, the number of columns is a multiple of the number of discrete combinations
of values related to the selected pivot columns. If a report pivots on source and medium,
and there are 10 cities and 5 pages, the result will have 50 metric columns; the number
of cells in the report is also a multiple of these columns, by the related report-level
dimensions (landing page, in the example below).


```python
batch = googleanalytics.ReportBatch(view_id)
report = batch.cursor  # access the "current" report

# all pageviews to the blog
blog = report.build_dimension_filter("ga:pagePath", '^/blog/')

report.add_dimension("ga:landingPage")

# The report with a pivot (which will also be segmented...)
report.add_pivot(report.build_pivot_clause(
    [report.build_metric_clause("ga:pageviews")], 
    [
        report.build_dimension_clause("ga:city"),  # pivot column
        report.build_dimension_clause("ga:pagePath")  # pivot column
    ],
    dim_filters=[
        report.build_dimension_filter_clause(blog)
    ]
))
```

## Fetching Results

A `ReportBatch` provides a `fetch` method to execute the queries against Google's
API, and iterates through the reports with a `ReportProxy` object. 

The `fetch` method requires an authenticated `GoogleAnalyticsReportingService` 
instance, and can optionally accept a series (as subsequent arguments) of 
Google Analytics view IDs; this allows you to issue the same batch of reports
against multiple views. 

The example below outlines full iteration of the reporting results. With 
sophisticated querying/reporting, the resulting structures can become quite complex.
The `ReportProxy` layer attempts to surface a consistent structure, relating
dimensions and metrics by name to their associated (numeric) values in the reports.

```python
from webapptitude import googleanalytics
from webapptitude import oauthkit

batch = googleanalytics.ReportBatch(view_id)
# Construct your reports...

auth = oauthkit.web()  # OAuth credentials factory.
ga = GoogleAnalyticsReportingService(auth)  # the Google Analytics proxy.

for report in batch.fetch(ga):  # Iterate through the reports in the batch.
    for dimensions, metrics, pivots in report.rows:
        # Each iteration here is a row of the given report.
        print "(begin row)"
        for dname, dvalue in dimensions:
            print "Dimension (%s = %r)" % (dname, dvalue)
        for mname, mvalue in metrics:
            print "Metric (%s = %r)" % (mname, mvalue)
        for piv in (pivots or []):
            # dimension combination, metric name, metric value
            dims, met, val = piv 
            print "Pivot Metric (%s = %r) for %r" % (met, val, dims)


```

The relation of named properties (metrics, dimensions) to their values is handled
through the `rows` method of the `ReportProxy` object. If alternative means of
interrogating the structure would be desirable, I recommend reviewing that `rows`
method for reference.


## References

Oh boy, the references. Google's documentation on this HTTP API is pretty dense.

This document attempted to distill the meaningful parts of it, however I'm sure 
there are some important details I've overlooked in outlining this functionality.
Good luck. 

- https://developers.google.com/analytics/devguides/reporting/core/v4/basics#response_body
- https://developers.google.com/analytics/devguides/reporting/core/v4/rest/v4/reports/batchGet#sequencesegment
- https://developers.google.com/analytics/devguides/reporting/core/v4/rest/v4/reports/batchGet#metrictype
- https://developers.google.com/resources/api-libraries/documentation/analyticsreporting/v4/python/latest/index.html
- https://developers.google.com/resources/api-libraries/documentation/analytics/v3/python/latest/index.html
- https://developers.google.com/analytics/devguides/reporting/
- https://developers.google.com/analytics/devguides/reporting/core/v4/advanced#dimensions
- https://developers.google.com/analytics/devguides/reporting/core/v4/parameters
- https://developers.google.com/analytics/devguides/reporting/core/v3/segments#operators
- https://developers.google.com/analytics/devguides/reporting/core/v3/reference#filters
- https://developers.google.com/analytics/devguides/reporting/core/dimsmets
- https://developers.google.com/analytics/devguides/config/mgmt/v3/


