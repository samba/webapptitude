# HTTP Digest Authentication

Webapptitude enables your application to support _users_ who aren't part of the
Google Accounts infrastructure (or Google Apps domain), with reasonable security,
but allowing HTTP Authentication to use the `Digest` model.

This can be especially useful for API services, where user credentials usually
won't relate to a real Google account.

Example shows it best:

```python
from webapptitude.httpauth import DigestAuthenticatedRequestHandler
from webapptitude.httpauth import authenticate

# create an application instance...

# NOTE: this sub-class arrangement is optional. The @authenticate decorator
# will automatically monkey-patch the request handler as needed.
class SecureRequestHandler(DigestAuthenticatedRequestHandler):
    
    # Requires user to authenticate via HTTP browser before passing the request.
    @authenticate("yourdomain.com")  # the auth realm is 'yourdomain.com' here.
    def get(self):
        return "You're authorized!"

```

To authorize a user, credentials are stored (securely) in the AppEngine Datastore. Example follows.

```python
from google.appengine.api import users


# This class is also an extension of `webapp2_extras.appengine.auth.models.User`
class User(webapptitude.httpauth.Authorization):
    pass  # extend properties as you will...


# Create a user entry. This can be handled in several ways. This example
# expects that you're attaching credentials to an existing Google account.
# Note that the `webapp2_extras.appengine.auth.models.User` model permits each
# user entry to have multiple (namespaced) usernames that may not be similar.
created, user = User.create_user('google:' + users.get_current_user().email())


# Define an HTTP username and password for this user.
# The @authenticate decorator above will now allow clients to issue requests
# with these credentials...
user.store_digest('http-username', 'http-password', 'yourdomain.com')
```
