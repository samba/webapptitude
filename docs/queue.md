# Deferred Workloads by Queue

Some workloads in analytics and (big) data applications require unbounded, 
queued tasks. Sometimes the size of that queue cannot be known in advance.
This library enables those workflows to be modeled in an intuitive, procedural
fashion.

1. [Comparison to Other Works](#comparison-to-other-works)
2. [Working with Flows, injectible queues](#working-with-flows-injectible-queues)
3. [Example Code](#example-code)
4. [Limitations](#limitations)
5. [Credits](#credits)

### Example Use-case

The Google Analytics API, for very large properties, may require several page 
iterations to retrieve the full content of a report. The number of pages to 
retrieve cannot be known in advance, and each must be requested with an updated 
`pageToken` that is only available on the page immediately preceding.

## Comparison to Other Works

Naturally many applications can leverage AppEngine's existing Task Queue model,
where a fixed queue handler can constitute the business logic to handle each
request, and potentially enqueue more. This library offers an alternative for
workloads which should be expressed more procedurally.

A few other developers have approached the same challenges in various ways. For
the simplest cases, the approach provided by `webapptitude.queue` may offer no 
significant advantages.

The major differences come from two key considerations:

- Workflows have state, sometimes entailing large datasets.
- Some workflows extend unpredictably (usually iteratively) in specific steps.

With these in mind, `webapptitude.queue` offers two essential behaviors:

- Every task can add to the flow's state, or access it directly.
- Every task can "prefix" additional tasks into the queue when needed.

At this time, flows are executed in series; this simplifies handling of state. 
(Notes on [parallelism](#parallelism) below.)

Parts of this module's interface and some of its logic are also derived from
the [waterf library](https://github.com/kaste/waterf).


## Working with _Flows_, injectible queues

The concept is that a "task" in the queue may need to schedule its own
follow-on tasks before an eventual completion task should run. Each task can
therefore "expand" into multiple as needed.

When a task behaves as a generator (using `yield`), it should:

- yield records/pages/etc, to add to the flow's resultset.
- yield additional tasks, to run next (before the following phase).

When a task behaves as a function (using `return`), it should:

1. return non-function values (not `None`) to replace (_override_) the current
   resultset; this enables _transforms_ on workflow state.
2. return a function, which will then be called with the `ndb.Model` instance
   that retains the flow's state.

As a convenience, a method `queue.stateful_task` simplifies this #2 case, so
a function can be provided without an interior return, as in the example with
`data_collect` farther below.

In advanced cases, it may be necessary to prepare a task from a basic function, 
which returns a generator, or another task; these cases are also supported.

In code, this would look like so:

```python
from webapptitude import queue

def step1(page_number, stop=False):
    # add a result to the workflow's state
    yield (page_number, fetch_page(page_number)) 
    if another_page_exists():
        # enqueue another page
        yield queue.task(step1, page_number + 1, stop=False)
    
def step2(): pass  # TODO: define your workflow steps.
def step3(): pass  # TODO: define your workflow steps.

queue.queue([
    
    # all args pass through to given funtions
    queue.task(step1, 0, stop=False),  # expands to multiple
    # additional instances of step1 may run here...

    # continue the flow with these steps...
    queue.task(step2, ...),
    queue.task(step3, ...)

    ], name="this_task_name",  # must be unique, optional (will autogenerate)
       fail=failure_handler).enqueue()

```


## Example Code

The following is taken from the current test suite. 

1. The `read_page` method would retrieve one entry per execution. When 
   additional entries are available, it schedules another iteration. 
2. The `data_collect` method retrieves the full set of records produced by the
   `read_page` method


```python
from webapptitude import queue

def read_page(index, data):
    if len(data) > index:
        # add an individual record to the series; this appends an entry to
        # the workflow's state.
        yield data[index]
        # a follow-on task (would-be recursion, yay!) to be executed
        # straight away, before moving to the next phase of the queue.
        yield queue.task(read_page, index + 1, data)


def data_collect(entry):
    # Because this function is wrapped in queue.stateful_task(), its only
    # argument will be the Flow entry for this particular workflow series.
    data = entry.state  # all records previously obtained.
    keys, values = [], []
    for record in data:
        keys.extend(record.keys())
        values.extend(record.values())
    # returning here overrides existing state.
    return keys, values

records = [{'one': 1}, {'two': 2}, {'three': 3}]

flow = queue.queue([ queue.task(read_page, 0, records),  # will expand
                     queue.stateful_task(data_collect) ])

flow.enqueue()  # and it shall be done


# Eventually, upon completion:
>>> flow.result   # short for `queue.Flow.get_by_id(flow.task_id).state`
(['one', 'two', 'three'], [1, 2, 3])

```

## Limitations

Some types of _callable_ task handlers may not be serializable for queuing. 
As a general rule, simple module-space functions are preferred, however 
callable-objects and object-methods often work too. Google has outlined the
[actual serialization limitations](https://cloud.google.com/appengine/articles/deferred?csw=1#limitations-of-the-deferred-library) reasonably well. This
`queue` module makes efforts to accommodate many callable variations.


### _Parallelism_

Currently task queues are processed entirely serially. 

Nothing in the `queue` library directly supports parallelizing a single workflow, 
however multiple flows can be dispatched into AppEngine's scheduler, which may 
run them simultaneously. This could permit parallel execution of discrete 
segments of otherwise-similar workflows. Future work in this library may enable 
parallelism within a given workflow.

## Credits

Thanks to `kaste` for the inspiration in [waterf](https://github.com/kaste/waterf).
