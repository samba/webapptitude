# A utility library for Google AppEngine

[![build status](https://gitlab.com/samba/webapptitude/badges/master/build.svg)](https://gitlab.com/samba/webapptitude/commits/master)
[![pypi package](https://img.shields.io/pypi/v/webapptitude.svg)](https://pypi.python.org/pypi/webapptitude)
[![pypi downloads](https://img.shields.io/pypi/dm/webapptitude.svg)](https://pypi.python.org/pypi/webapptitude)

This library provides a number of common utilities, decorators, and conveniences
to simplify development of applications on Google AppEngine (in Python).

The problem: webapp2 provides a fantastic number of very flexible features, but
they often require a degree of custom code that just gets annoying, especially
considering Python's refrain of "don't repeat yourself."

This library enables many of those features by default, with sensible default
settings, and provides easy access to the essential interfaces.

## Components

This project's documentation is broken into several components. 

1. [Basic Usage](docs/basic.md) Application fundamentals
2. [JSON API extensions](docs/json.md)  Building an API service
3. [HTTP Digest Authentication](docs/digest.md)  Secure authentication within the app
4. [Google Analytics API kit](docs/googleanalytics.md)  Reusable components for the Analytics V4 API
5. [BigQuery API kit](docs/bigquery.md)  Sensible object abstraction on BigQuery datasets
6. [Testing Your Application](docs/testing.md)  Simplifying the test architecture
7. [Go with the Flow](docs/queue.md)  Procedural, stateful out-of-band workflow tooling.


## Installation

1. Install Google AppEngine from [AppEngine Downloads][2]
2. In your project directory, install the Webapptitude library to your local vendor libraries path (often `lib`).
3. Configure your project's [`appengine_config.py`][1] to load vendor libraries.

### Step 2: installing vendor library

Perhaps the simplest approach: install the library in a one-shot command to the
target `lib` directory.

```shell
pip install -t lib webapptitude
```

It's recommended to append the package to your projects `requirements.txt` file
so `pip` can manage them reporducibly in the future.

Your `requirements.txt` file would then look something like this:

```
# other libraries you might need...
Markdown==2.5.2
webapptitude
```

You can then install all required packages more reliably:

```shell
pip install -t lib -r requirements.txt
```

## Step 3: configure your application

In your [`appengine_config.py`][1], placed in project root, the following code enables
your application to use libaries/modules from a path `lib` within the project.
This is generally required for applications which depend on third-party libraries
that Google doesn't already provide.

```python
from google.appengine.ext import vendor

# Add any libraries installed in the "lib" folder.
vendor.add('lib')

```

AppEngine also provides several versions of various libraries. This can cause
confusion when a feature in a newer version is absent from an older version, and
the application may (automatically) select an older version of the library. 
Because Webapptitude aims to address modern development needs, we prefer to
target the latest versions of these dependencies.

Recommend adding the following specification to application configs:

```yaml
libraries:
- name: yaml
  version: latest
- name: webob
  version: latest
- name: webapp2
  version: latest
- name: jinja2
  version: latest
- name: django
  version: latest
```

The application configuration files include:

- `app.yaml`
- all modules noted by `dispatch.yaml`, their respective yaml files.


[1]: https://cloud.google.com/appengine/docs/python/tools/appengineconfig
[2]: https://cloud.google.com/appengine/downloads

